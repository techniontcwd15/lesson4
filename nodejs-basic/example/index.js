var http = require('http');

var server = http.createServer(function (req, res) {

  res.writeHead(200, {'Content-Type': 'text/plain'});

  switch (req.url) {
    case '/':
      res.write('<h1>hi<h1>');
      break;
    case '/bye':
      res.write('bye');
      break;
    default:
      // 404
  }
  res.end();
});

server.listen(8081);
console.log('you can listen at http://localhost:8081');
