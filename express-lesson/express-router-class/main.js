var express = require('express');
var app = express();
var birds = require('./bird');

app.use('/birds', birds);

app.listen('3000');
