var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var mongoose   = require('mongoose');

mongoose.connect('mongodb://class1:class1@ds029595.mongolab.com:29595/class1');

app.use(bodyParser.json());

var Schema       = mongoose.Schema;

var TestSchema   = new Schema({
    name: String
});

var Test = mongoose.model('Test', TestSchema);


app.get('/', function (req, res) {
  res.json({hello: 'world'});
});

app.post('/test', function (req, res) {
  var test = new Test();
  test.name = req.body.name;
  test.save(function(err) {
      if (err) res.send(err);

      res.json(test);
  });
});


var server = app.listen(3000);
