var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());

app.post('/', function(req, res) {
  console.log(req.body);
  if(req.body.name === 'amit' && req.body.password === '123456') {
    res.end('{"hello":"world"}');
  } else {
    res.end('error');
  }
});

app.get('/bye', function(req, res) {
  res.end('bye');
})

app.listen(8080);
