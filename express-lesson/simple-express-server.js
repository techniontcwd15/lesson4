var express = require('express');
var bodyParser = require('body-parser');
var items = require('./posts.json');

var app = express();
app.use(bodyParser.json());

console.log(items);


app.get('/say/:something', function (req, res) {
  console.log(req.params.something);
  res.send('hello');
  res.end();
});

app.post('/hello', function(req, res) {
  console.log(req.body);
  console.log(req.headers);
  if(req.headers.xyz === '42') {
    res.send('you found it');
  }
  res.end();
});


var server = app.listen(3000);
