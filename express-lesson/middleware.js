var express = require('express');
var app = express();

app.use(function(req, res, next) {
  console.log('middleware all');
  // res.end('end of middleware');
  next();
});

app.use('/item/:id',function(req, res, next) {
  console.log('middleware item');
  next();
});

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/item/:id', function (req, res) {
  res.send(req.params.id);
});

// app.get('/item/:id', function (req, res) {
//   console.log('item2');
//   res.send('Item2');
// });

var server = app.listen(3000);
