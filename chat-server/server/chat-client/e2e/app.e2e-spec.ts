import { ChatClientPage } from './app.po';

describe('chat-client App', () => {
  let page: ChatClientPage;

  beforeEach(() => {
    page = new ChatClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
