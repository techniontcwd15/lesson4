import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class MessagesService {

  constructor(private http: Http) { }

  public getMessage() {
    return this.http.get('/api/messages').map(res=>res.json());
  }

}
