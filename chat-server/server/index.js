var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/chat');

var Message = mongoose.model('Message', { name: String, text: String });

var app = express();
app.use(express.static('chat-client/dist'))

app.use(bodyParser.json());

app.get('/api/messages', function (req, res) {
  Message.find((err, messages) => {
    res.send(messages);
    res.end();
  })
});

app.post('/api/messages', function(req, res) {
  var message = new Message();

  message.name = req.body.name;
  message.text = req.body.text;

  message.save((err) => {
    if(err)
      console.log(err);

    res.end();
  })
});

app.put('/api/messages/:id', function(req, res) {
  console.log(req.params.id);

});

app.delete('/api/messages/:id', function(req, res) {
  console.log(req.params.id);

});

app.listen(3000);
